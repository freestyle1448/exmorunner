package online.cryptopie.tasks;

import online.cryptopie.app.Locker;
import online.cryptopie.models.AutomaticGate;
import online.cryptopie.models.Gate;
import online.cryptopie.models.transaction.History;
import online.cryptopie.models.transaction.Transaction;
import online.cryptopie.repositories.AutomaticGatesRepository;
import online.cryptopie.repositories.GatesRepository;
import online.cryptopie.repositories.ManualRepository;
import online.cryptopie.service.ExmoService;
import org.bson.types.ObjectId;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import static online.cryptopie.app.Application.HASH_PAS;
import static online.cryptopie.app.Locker.*;
import static online.cryptopie.models.transaction.Status.IN_PROCESS;
import static online.cryptopie.models.transaction.Status.WAITING;

@Component
@Async
public class ExmoTasks {
    private final ExmoService exmoService;
    private final ManualRepository manualRepository;
    private final AutomaticGatesRepository automaticGatesRepository;
    private final GatesRepository gatesRepository;
    private final Locker locker = Locker.getInstance();

    public ExmoTasks(ExmoService exmoService, ManualRepository manualRepository, AutomaticGatesRepository automaticGatesRepository, GatesRepository gatesRepository) {
        this.exmoService = exmoService;
        this.manualRepository = manualRepository;
        this.automaticGatesRepository = automaticGatesRepository;
        this.gatesRepository = gatesRepository;
    }

    private static String bytesToHex(byte[] hash) {
        StringBuilder hexString = new StringBuilder();
        for (byte aHash : hash) {
            String hex = Integer.toHexString(0xff & aHash);
            if (hex.length() == 1) hexString.append('0');
            hexString.append(hex);
        }
        return hexString.toString();
    }

    private Transaction genHash(Transaction transaction) {
        if (transaction.getTransactionNumber() == null)
            transaction.setTransactionNumber((long) 0);


        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        assert digest != null;

        byte[] encodedhash = digest.digest(
                String.format("%s|%s|%s|%s|%s",
                        transaction.getPair(), transaction.getDate(), //TODO getCardNumber
                        transaction.getPrice(), transaction.getQuantity(), HASH_PAS).getBytes(StandardCharsets.UTF_8));
        transaction.setHash(bytesToHex(encodedhash));
        return transaction;
    }

    public static long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
        long diffInMillies = date2.getTime() - date1.getTime();
        return timeUnit.convert(diffInMillies, TimeUnit.MILLISECONDS);
    }

    @Scheduled(fixedRate = 10000)
    public void pay() {
        AutomaticGate automaticGates = automaticGatesRepository.findByGroupName("EXMO");
        List<ObjectId> exmoGates = new ArrayList<>(automaticGates.getGates());
        List<Transaction> transactions = manualRepository.findByStatusEXMOTransactions(exmoGates, WAITING);

        if (!transactions.isEmpty()) {
            if (locker.isNotLocked(PING_LOCK) && locker.isNotLocked(PAY_LOCK)) {
                locker.lock(PAY_LOCK);

                for (Transaction transaction : transactions) {
                    transaction.setStatus(IN_PROCESS);

                    if (transaction.getHistoryList() == null) {
                        transaction.setHistoryList(new ArrayList<>());
                        transaction.getHistoryList().add(History.builder().date(new Date()).prevStatus(WAITING).curStatus(IN_PROCESS).build());
                    }

                    Optional<Gate> gateOptional = gatesRepository.findById(transaction.getGateId());
                    Gate gate = null;
                    if (gateOptional.isPresent())
                        gate = gateOptional.get();
                    assert gate != null;
                    transaction.setOrderTimeout(gate.getDefaultExmoTimeout());

                    if (transaction.getHash().equals(genHash(transaction).getHash())) {
                        exmoService.saveTransaction(transaction);
                        exmoService.createExmoRequest(transaction);
                    } else {
                        exmoService.declineTransaction(transaction);
                    }
                }
            }
        }

        locker.unlock(PAY_LOCK);
    }

    @Scheduled(fixedRate = 10000)
    public void check() {
        AutomaticGate automaticGates = automaticGatesRepository.findByGroupName("EXMO");
        List<ObjectId> exmoGates = new ArrayList<>(automaticGates.getGates());
        List<Transaction> transactions = manualRepository.findByStatusEXMOTransactions(exmoGates, IN_PROCESS);

        if (!transactions.isEmpty()) {
            if (locker.isNotLocked(PING_LOCK)) {
                locker.lock(CONFIRM_LOCK);

                for (Transaction transaction : transactions) {
                    if (transaction.getHash().equals(genHash(transaction).getHash())
                            && getDateDiff(transaction.getDate(), new Date(), TimeUnit.SECONDS) <= transaction.getOrderTimeout()) {
                        exmoService.saveTransaction(transaction);
                        exmoService.statusRequest(transaction);
                    } else {
                        exmoService.declineTransaction(transaction);
                    }
                }
                locker.unlock(CONFIRM_LOCK);
            }
        }
    }
}
