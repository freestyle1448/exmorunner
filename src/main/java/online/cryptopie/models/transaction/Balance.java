package online.cryptopie.models.transaction;

import lombok.*;
import online.cryptopie.models.Currency;
import online.cryptopie.repositories.CurrenciesRepository;
import org.javamoney.moneta.CurrencyUnitBuilder;
import org.javamoney.moneta.Money;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.money.CurrencyUnit;
import javax.money.MonetaryAmount;
import javax.money.UnknownCurrencyException;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString

@Component
public class Balance {
    private Number amount;
    private String currency;
    private static CurrenciesRepository nRep;
    @Autowired
    private CurrenciesRepository currenciesRepository;

    public static MonetaryAmount getMonetary(Balance balance) {
        MonetaryAmount money;

        try {
            money = Money.of(balance.getAmount().doubleValue(), balance.getCurrency());
        } catch (UnknownCurrencyException e) {
            Currency c = nRep.findByCurrency(balance.getCurrency());
            CurrencyUnit cur = CurrencyUnitBuilder.of(balance.getCurrency(), "default")
                    .setNumericCode(c.getCode())
                    .setDefaultFractionDigits(3)
                    .build();
            money = Money.of(balance.getAmount().doubleValue(), cur);
        }

        return money;
    }

    @PostConstruct
    public void init() {
        Balance.nRep = currenciesRepository;
    }

    public static Balance getBalance(MonetaryAmount amount) {
        return Balance.builder().amount(amount.getNumber().doubleValue()).currency(amount.getCurrency().getCurrencyCode()).build();
    }
}
