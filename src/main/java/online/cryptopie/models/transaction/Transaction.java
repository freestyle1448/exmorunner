package online.cryptopie.models.transaction;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

@Document(collection = "transactions")
public class Transaction {
    @Id
    private ObjectId id;
    private String hash;
    private String sign;
    private String salt;
    private Date date;
    private String type;
    private Integer status;
    private Balance commission;
    private String note;
    private Integer gateType;
    private List<History> historyList;
    private Long exmo_order_id;
    private String exmoType;
    private Integer orderTimeout;
    private String pair;
    private Double quantity;
    private Double price;
    private ObjectId toGate;
    private ObjectId fromGate;
    private String fromAccount;
    private String toAccount;
    private ObjectId gateId;
    private Long transactionNumber;
    /*private String accountId;




    private Balance amount;


    private Balance finalAmount;
    ;

    private Balance toAmount;
    private Balance fromAmount;


    private String acceptId;

    private String userPhone;
    private String cardNumber;

    private Balance systemAmount;
    private Number rate;
    private Number systemRate;
    private Number limit;
    private Integer remoteStatus;


    private Long split_id;

    */
}
