package online.cryptopie.models.orderStatus;

import com.google.gson.*;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Map;
import java.util.Set;

public class OrderStatusDeserializer implements JsonDeserializer<OrderStatus> {
    @Override
    public OrderStatus deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        final JsonObject jsonElement = json.getAsJsonObject();
        final Set<Map.Entry<String, JsonElement>> entries = jsonElement.entrySet();
        final Map.Entry<String, JsonElement> next = entries.iterator().next();

        int count = 0;
        ArrayList<Status> arrayList = new ArrayList<>();
        try {


            while (next.getValue().getAsJsonArray().get(count) != null) {
                final JsonElement element = next.getValue().getAsJsonArray().get(count);
                count++;

                final JsonObject asJsonObject = element.getAsJsonObject();
                final Set<Map.Entry<String, JsonElement>> entries1 = asJsonObject.entrySet();
                Status status = new Status();

                entries1.forEach(stringJsonElementEntry -> {
                    switch (stringJsonElementEntry.getKey()) {
                        case "trade_id":
                            status.setTrade_id(stringJsonElementEntry.getValue().getAsLong());
                            break;
                        case "date":
                            status.setDate(stringJsonElementEntry.getValue().getAsLong());
                            break;
                        case "type":
                            status.setType(stringJsonElementEntry.getValue().getAsString());
                            break;
                        case "pair":
                            status.setPair(stringJsonElementEntry.getValue().getAsString());
                            break;
                        case "order_id":
                            status.setOrder_id(stringJsonElementEntry.getValue().getAsLong());
                            break;
                        case "quantity":
                            status.setQuantity(stringJsonElementEntry.getValue().getAsFloat());
                            break;
                        case "price":
                            status.setPrice(stringJsonElementEntry.getValue().getAsFloat());
                            break;
                        case "amount":
                            status.setAmount(stringJsonElementEntry.getValue().getAsFloat());
                            break;
                    }
                });

                arrayList.add(status);
            }
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
        }
        return OrderStatus.builder().pair(arrayList).build();
    }
}
