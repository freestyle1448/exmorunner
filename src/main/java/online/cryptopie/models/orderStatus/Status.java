package online.cryptopie.models.orderStatus;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Status {
    private Long trade_id;
    private Long date;
    private String type;
    private String pair;
    private Long order_id;
    private Float quantity;
    private Float price;
    private Float amount;
}
