package online.cryptopie.models.orderCreate;

public final class OrderType {
    public static final String BUY = "buy";
    public static final String SELL = "sell";
    public static final String MARKET_BUY = "market_buy";
    public static final String MARKET_SELL = "market_sell";
    public static final String MARKET_BUY_TOTAL = "market_buy_total";
    public static final String MARKET_SELL_TOTAL = "market_sell_total";
}
