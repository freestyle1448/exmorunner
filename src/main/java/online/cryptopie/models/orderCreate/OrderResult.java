package online.cryptopie.models.orderCreate;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OrderResult {
    private Boolean result;
    private String error;
    private Long order_id;
}
