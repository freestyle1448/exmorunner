package online.cryptopie.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import online.cryptopie.dto.BalanceDTO;
import online.cryptopie.models.transaction.Commission;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

@Document(collection = "gates")
public class Gate {
    @Id
    private ObjectId id;
    private String name;
    private Integer type;
    private ObjectId currency;
    private List<ObjectId> transactions;
    private Commission commission;
    private BalanceDTO commissionBalance;
    private BalanceDTO balance;
    private String account;

    private Integer defaultExmoTimeout;
}
