package online.cryptopie.models;

import online.cryptopie.models.transaction.Transaction;

import java.util.HashMap;


public final class ExmoCreator {
    public static HashMap<String, String> createOrder(Transaction transaction) {
        HashMap<String, String> order = new HashMap<>();

        order.put("pair", transaction.getPair());
        order.put("quantity", transaction.getQuantity().toString());
        order.put("price", transaction.getPrice().toString());
        order.put("type", transaction.getExmoType());
        return order;
    }

    public static HashMap<String, String> createStatusRequest(Transaction transaction) {
        HashMap<String, String> status = new HashMap<>();

        status.put("pair", transaction.getPair());
        status.put("limit", String.valueOf(100));
        status.put("offset", String.valueOf(0));

        return status;
    }
}
