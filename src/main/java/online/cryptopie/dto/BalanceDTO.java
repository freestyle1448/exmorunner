package online.cryptopie.dto;

import lombok.*;
import org.bson.types.ObjectId;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class BalanceDTO {
    private Number amount;
    private ObjectId currency;
}