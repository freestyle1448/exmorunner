package online.cryptopie.app;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;

public class ExmoLogger {
    private final static String PATH = "exmolog.log";//"/home/admin/exmolog.log"
    private static ExmoLogger instance;

    private ExmoLogger() {
        File file = new File(PATH);
        if (!file.exists()) {
            try {
                //noinspection ResultOfMethodCallIgnored
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static synchronized ExmoLogger getInstance() {
        if (instance == null) {
            instance = new ExmoLogger();
        }
        return instance;
    }

    public void printMes(String mes) {
        try (FileWriter writer = new FileWriter(PATH, true)) {
            Date date = new Date();
            writer.write(date + " " + mes + "\r\n");
            writer.flush();
        } catch (IOException ex) {

            System.out.println(ex.getMessage());
        }

    }
}
