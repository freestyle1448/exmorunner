package online.cryptopie.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import online.cryptopie.app.ExmoLogger;
import online.cryptopie.models.Account;
import online.cryptopie.models.Exmo;
import online.cryptopie.models.ExmoCreator;
import online.cryptopie.models.Gate;
import online.cryptopie.models.orderCreate.OrderResult;
import online.cryptopie.models.orderStatus.OrderStatus;
import online.cryptopie.models.orderStatus.OrderStatusDeserializer;
import online.cryptopie.models.orderStatus.Status;
import online.cryptopie.models.transaction.Balance;
import online.cryptopie.models.transaction.History;
import online.cryptopie.models.transaction.Transaction;
import online.cryptopie.repositories.AccountsRepository;
import online.cryptopie.repositories.GatesRepository;
import online.cryptopie.repositories.ManualRepository;
import online.cryptopie.repositories.TransactionsRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.Optional;

import static online.cryptopie.models.transaction.Status.DENIED;
import static online.cryptopie.models.transaction.Status.SUCCESS;

@Service
public class ExmoServiceImpl implements ExmoService {
    private final ExmoLogger exmoLogger = ExmoLogger.getInstance();
    private final ManualRepository manualRepository;
    private final Exmo exmo = new Exmo("", ""); //TODO добавить key и secret
    private final TransactionsRepository transactionsRepository;
    private final GatesRepository gatesRepository;
    private final AccountsRepository accountsRepository;

    public ExmoServiceImpl(ManualRepository manualRepository, TransactionsRepository transactionsRepository, GatesRepository gatesRepository, AccountsRepository accountsRepository) {
        this.manualRepository = manualRepository;
        this.transactionsRepository = transactionsRepository;
        this.gatesRepository = gatesRepository;
        this.accountsRepository = accountsRepository;
    }


    @Override
    public int createExmoRequest(Transaction transaction) {
        Gson gson = new Gson();
        String result = exmo.Request("order_create", ExmoCreator.createOrder(transaction));
        OrderResult resultObject = gson.fromJson(result, OrderResult.class);
        if (resultObject.getError().isEmpty()) {
            transaction.setExmo_order_id(resultObject.getOrder_id());
            saveTransaction(transaction);
            exmoLogger.printMes("CREATE order in createExmoRequest " + transaction.getExmo_order_id());
            return 0;
        }

        exmoLogger.printMes("ERROR in createExmoRequest " + resultObject.getError());
        return -1;
    }

    @Override
    public int statusRequest(Transaction transaction) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(OrderStatus.class, new OrderStatusDeserializer());
        Gson gson = gsonBuilder.create();

        String result = exmo.Request("user_trades", ExmoCreator.createStatusRequest(transaction));
        OrderStatus status = gson.fromJson(result, OrderStatus.class);

        if (status != null)
            if (status.getPair() != null && status.getPair().size() > 0) {
                for (Status exTr : status.getPair()) {
                    if (exTr.getOrder_id().equals(transaction.getExmo_order_id()))
                        confirmTransaction(transaction);
                }
            }

        return 1;
    }

    @Override
    public void saveTransaction(Transaction transaction) {
        transactionsRepository.save(transaction);
    }

    @Transactional
    public void confirmTransaction(Transaction transaction) {
        if (transaction != null) {
            Gate gate = null;
            Optional<Gate> gateOptional = gatesRepository.findById(transaction.getFromGate());
            Account toAccount = accountsRepository.findByAccountId(transaction.getToAccount());
            if (gateOptional.isPresent())
                gate = gateOptional.get();

            if (toAccount != null) {
                if (gate != null) {
                    transaction.getHistoryList().add(History.builder()
                            .date(new Date())
                            .prevStatus(transaction.getStatus())
                            .curStatus(SUCCESS)
                            .build());

                    manualRepository.findAndModifyAccountAdd(transaction.getToAccount(),
                            Balance.builder()
                                    .amount(transaction.getQuantity())
                                    .currency(transaction.getPair().substring(0, 3))
                                    .build());
                    manualRepository.findAndModifyGateAdd(transaction.getToGate(),
                            Balance.builder()
                                    .amount(transaction.getQuantity())
                                    .currency(transaction.getPair().substring(0, 3))
                                    .build());
                   
                    manualRepository.findAndUpdateGateCommissionAndAccount(gate.getId(), transaction.getCommission());
                    transaction.setStatus(SUCCESS);
                    transactionsRepository.save(transaction);

                    exmoLogger.printMes("Transaction accepted");
                } else {
                    exmoLogger.printMes("IN TRANSACTION SAVE not enough money on gate or gate with id dont find");
                }
            }
        } else {
            exmoLogger.printMes("IN TRANSACTION SAVE transaction dont find");
        }
    }

    @Transactional
    public int declineTransaction(Transaction transaction) {
        if (transaction != null) {
            if ((manualRepository.findAndModifyAccountAdd(transaction.getFromAccount(), Balance.builder()
                    .amount(transaction.getPrice() + transaction.getCommission().getAmount().doubleValue())
                    .currency(transaction.getPair().substring(4, 7))
                    .build()) != null)
                    && (manualRepository.findAndModifyGateAdd(transaction.getFromGate(), Balance.builder()
                    .amount(transaction.getPrice() + transaction.getCommission().getAmount().doubleValue())
                    .currency(transaction.getPair().substring(4, 7))
                    .build()) != null)) {
                if (transaction.getHistoryList() == null) {
                    transaction.setHistoryList(new ArrayList<>());
                    transaction.getHistoryList().add(History.builder()
                            .date(new Date())
                            .prevStatus(transaction.getStatus())
                            .curStatus(DENIED)
                            .build());
                }

                transaction.setStatus(DENIED);
                transactionsRepository.save(transaction);

                exmoLogger.printMes("Transaction declined");
                return 1;
            } else {
                exmoLogger.printMes("IN TRANSACTION SAVE gate with id dont find");
                return -1;
            }
        } else {
            exmoLogger.printMes("IN TRANSACTION SAVE transaction dont find");
            return -1;
        }
    }
}
