package online.cryptopie.repositories;

import online.cryptopie.models.transaction.Transaction;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface TransactionsRepository extends MongoRepository<Transaction, ObjectId> {
    Transaction findByIdAndStatus(ObjectId transactionId, Integer status);

    List<Transaction> findAllByStatus(Integer status);
}
