package online.cryptopie.repositories;

import online.cryptopie.models.Account;
import online.cryptopie.models.Currency;
import online.cryptopie.models.Gate;
import online.cryptopie.models.transaction.Balance;
import online.cryptopie.models.transaction.Transaction;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Repository
public class ManualRepository {
    private final MongoTemplate mongoTemplate;
    private final CurrenciesRepository currenciesRepository;
    private final GatesRepository gatesRepository;
    private final TransactionsRepository transactionsRepository;

    @Autowired
    public ManualRepository(MongoTemplate mongoTemplate, CurrenciesRepository currenciesRepository
            , GatesRepository gatesRepository, TransactionsRepository transactionsRepository) {
        this.mongoTemplate = mongoTemplate;
        this.currenciesRepository = currenciesRepository;
        this.gatesRepository = gatesRepository;
        this.transactionsRepository = transactionsRepository;
    }

    public Gate findAndModifyGateSub(ObjectId gateId, Balance balance) {
        Currency currency = currenciesRepository.findByCurrency(balance.getCurrency());
        Query findAndModifyGate = new Query(Criteria.where("_id").is(gateId).and("balance.amount").gte(balance.getAmount().doubleValue()).and("balance.currency").is(currency.getId()));
        Update update = new Update();
        update.inc("balance.amount", -balance.getAmount().doubleValue());

        return mongoTemplate.findAndModify(findAndModifyGate, update, Gate.class);
    }

    public Account findAndModifyAccountAdd(String accountId, Balance balance) {
        Currency currency = currenciesRepository.findByCurrency(balance.getCurrency());
        Query findAndModifyAccount = new Query(Criteria.where("accountId").is(accountId).and("currency").is(currency.getId()));
        Update update = new Update();
        update.inc("balance.amount", balance.getAmount().doubleValue());

        return mongoTemplate.findAndModify(findAndModifyAccount, update, Account.class);
    }

    public Gate findAndModifyGateAdd(ObjectId gateId, Balance balance) {
        Currency currency = currenciesRepository.findByCurrency(balance.getCurrency());
        Query findAndModifyGate = new Query(Criteria.where("_id").is(gateId).and("balance.currency").is(currency.getId()));
        Update update = new Update();
        update.inc("balance.amount", balance.getAmount().doubleValue());

        return mongoTemplate.findAndModify(findAndModifyGate, update, Gate.class);
    }


    public Boolean findAndUpdateGateCommissionAndAccount(ObjectId gateId, Balance balance) {
        Currency currency = currenciesRepository.findByCurrency(balance.getCurrency());
        Query findAndModifyGate = new Query(Criteria.where("_id").is(gateId).and("commissionBalance.currency").is(currency.getId()));
        Update update = new Update();
        update.inc("commissionBalance.amount", balance.getAmount().doubleValue());

        Optional<Gate> gate = gatesRepository.findById(gateId);

        Query findAndModifyAccount = null;
        if (gate.isPresent())
            findAndModifyAccount = new Query(Criteria.where("accountId").is(gate.get().getAccount()).and("balance.currency").is(currency.getId()));

        Update updateAccount = new Update();
        updateAccount.inc("balance.amount", balance.getAmount().doubleValue());

        assert findAndModifyAccount != null;
        Account accountUpdated = mongoTemplate.findAndModify(findAndModifyAccount, updateAccount, Account.class);
        Gate updated = mongoTemplate.findAndModify(findAndModifyGate, update, Gate.class);

        return updated != null && accountUpdated != null;
    }

    public Currency findCurrencyByGateId(ObjectId gateId) {
        Optional<Gate> optionalGate = gatesRepository.findById(gateId);
        if (optionalGate.isPresent()) {
            Gate gate = optionalGate.get();
            Optional<Currency> optionalCurrency = currenciesRepository.findById(gate.getCurrency());
            if (optionalCurrency.isPresent()) {
                return optionalCurrency.get();
            }
        }

        return Currency.builder().build();
    }

    public List<Transaction> findByStatusEXMOTransactions(List<ObjectId> exmoGates, Integer status) {
        List<Transaction> transactions = transactionsRepository.findAllByStatus(status);

        List<Transaction> filteredTransactions = new ArrayList<>();
        if (!transactions.isEmpty())
            transactions.stream().filter(transaction -> {

                for (ObjectId exmoGateId : exmoGates) {
                    if (transaction.getGateId() != null)
                        if (transaction.getGateId().equals(exmoGateId))
                            return true;
                }
                return false;
            }).forEach(filteredTransactions::add);

        return filteredTransactions;
    }
}
