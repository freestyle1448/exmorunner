package online.cryptopie.repositories;

import online.cryptopie.models.Account;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface AccountsRepository extends MongoRepository<Account, ObjectId> {
    Account findByAccountId(String accountId);
}
